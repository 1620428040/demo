<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2018 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +----------------------------------------------------------------------
namespace plugins\post_comment\controller;
use cmf\controller\PluginBaseController;
use app\user\model\CommentModel;

class IndexController extends PluginBaseController{
    function index(){
        echo "ok";
    }
    //插入新的评论
    /*  $param=[
        'object_id'=>$article['id'],
        'table_name'=>'portal_post',
        'user_id',
        "full_name",
        "parent_id",
        "to_user_id",
        "content"
    ];  */
    public function insertComment(){
        $param=$_POST;
        if(!$param["content"]){
            $this->error("评论不能为空");
        }
        $user=cmf_get_current_user();
        if(!$user){
            $this->error("用户未登录");
        }
        $param["user_id"]=$user["id"];
        $param["full_name"]=$user["user_nickname"];
        $param["create_time"]=time();
        //TODO:插入"floor"楼层字段
        $model=new CommentModel();
        $model->save($param);
        $this->success("评论成功");
    }
}
