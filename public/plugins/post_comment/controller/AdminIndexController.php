<?php
namespace plugins\post_comment\controller;
use cmf\controller\PluginAdminBaseController;
use think\Db;

class AdminIndexController extends PluginAdminBaseController
{

    protected function _initialize()
    {
        parent::_initialize();
        $adminId = cmf_get_current_admin_id();//获取后台管理员id，可判断是否登录
        if (!empty($adminId)) {
            $this->assign("admin_id", $adminId);
        }
    }

    /**
     * 评论插件
     * @adminMenu(
     *     'name'   => '评论插件',
     *     'parent' => 'admin/Plugin/default',
     *     'display'=> true,
     *     'hasView'=> true,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '评论插件',
     *     'param'  => ''
     * )
     */
    public function index(){
        echo "index";
    }

    /**
     * 评论插件设置
     * @adminMenu(
     *     'name'   => '评论插件设置',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> true,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '评论插件设置',
     *     'param'  => ''
     * )
     */
    public function setting(){
        echo "setting";
    }

}
