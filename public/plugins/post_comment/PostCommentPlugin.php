<?php
namespace plugins\post_comment;
use app\user\model\CommentModel;
use cmf\lib\Plugin;

class PostCommentPlugin extends Plugin{
    public $info = [
        'name'=>'PostComment',
        'title'=>'评论',
        'description'=>'自己写的，与thinkcmf的POST配套的评论',
        'status'=>1,
        'author'=>'guodong',
        'version'=>'0.1',
        'demo_url'=>''
    ];
    public $hasAdmin = 1;//插件是否有后台管理界面

    public function install(){
        return true;
    }

    public function uninstall(){
        return true;
    }

    //实现的comment钩子方法
    /*  $param=[
        'object_id'=>$article['id'],
        'table_name'=>'portal_post',
        'object_title'=>$article['post_title'],
        'url'=>cmf_url_encode('portal/Article/index',array('id'=>$article['id'],'cid'=>$category['id'])),
        'user_id'=>$article['user_id']
    ];  */
    public function comment($param){
        $model=new CommentModel();
        $comments=$model->where(["object_id"=>$param["object_id"],"table_name"=>$param["table_name"]])->select();
        $this->assign("comments",$comments);
        $this->assign($param);
        return $this->fetch('widget');
    }
}