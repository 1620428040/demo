<?php
namespace Trade\Model;

class TopupModel extends PaymentModel{
    protected $tradeType="topup";

    public function generateOrder($uid,$params){
        $money=$params["money"];
        return [
            "title"=>"充值",
            "money"=>$money,
            "description"=>"充值钻石币",
            "trade_data"=>[ 
                "uid"=>$uid,
                "inmoney"=>$money
            ]
        ];
    }
    public function executionOrder($tradeData){
        $uid=$tradeData["uid"];
        $money=$tradeData["inmoney"];
        $userModel = M("Users");
        if(!$userModel->where(["id"=>$uid])->setInc('money',$money)){
            $this->error="更新余额失败";
            return false;
        }
        $user=$userModel->where(["id"=>$uid])->find();
        $topupModel = M("Topup");
        $tradeData["lastmoney"]=$user["money"];
        $tradeData["ptime"]=date("Y年m月d日 H:i分");
        if(!$topupModel->add($tradeData)){
            $this->error="更新数据失败";
            return false;
        }
        return true;
    }
    public function tradeWithApple($uid,$params){
        $product=[
            "z_8"=>5.6,
            "z_18"=>12.6,
            "z_28"=>19.6,
            "z_45"=>31.5,
            "z_68"=>47.6,
            "z_88"=>61.6,
            "z_198"=>138.6,
            "z_898"=>628.6,
        ];
        $money=$product[$params["product_id"]];
        if(!$money){
            $this->error="product不存在";
            return false;
        }

        $userModel = M("Users");
        $res=$userModel->where(["id"=>$uid])->setInc('money',$money);
        $user=$userModel->where(["id"=>$uid])->find();
        if(!$res){
            $this->error="充值失败";
            return false;
        }
        $topupModel = M("Topup");
        $data = array(
            'uid' => $user["id"],
            'inmoney' => $money,
            'lastmoney' => $user["money"],
            'ptime' => date("Y年m月d日 H:i分")
        );
        $res = $topupModel->add($data);
        if(!$res){
            $this->error="更新充值记录失败";
            return false;
        }
        return [
            "title"=>"充值",
            "money"=>$money,
            "description"=>"充值钻石币"
        ];
    }
}