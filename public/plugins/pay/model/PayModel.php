<?php
namespace plugins\pay\model;
use think\Model;
use think\Loader;

/**
 * 支付需要的调用第三方服务的基类
 */
class PayModel extends Model{
    public $tradeModel;
    private $sdkPath="";
    //查询接口
    public function query($trade_no=null,$out_trade_no=null){
        return [];
    }
    public function check($data,$type="notify"){
        return [];
    }
    public function verifyReturn($data){
        $result=$this->check($data,"return");
        if(!$result){
            return false;
        }
        return BaseModel::updateAsyncTrade($content["out_trade_no"],$content["transaction_id"]);
    }
    public function verifyNotify(){
        return $this->check($_POST);
    }
    public function replyNotify($result,$message=""){
        echo $result ? "success" : "fail";
        exit;
    }
    //转账
    public function transfer($params){
        return true;
    }
}