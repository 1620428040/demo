<?php
namespace plugins\pay\model;
use think\Model;
use think\Loader;

class AlipayModel extends PayModel{
    private $privateKey = "MIIEpAIBAAKCAQEAvB4aTjYScfzxgkB1q81+LIO0FVf/RDXia+giE48vg1zaanXUnNba4swNi9jipAOWcNmugEgE6zJdUD0aOyTKp5ti63IrYkB1MTVg8oazF1K+jc8r3IIg/0qWoVAXo7RVgQ/oEGkONs93oY/n30eINXXApBu7adcKjZ769wPfzV7VsK01VLNRq2CKCYR+nL7EAYBsQzTh7IOVFQfUiyZKwjppMGa27gnLcNiXPrxES8G7eMVoxzg4EbDa+5MsXU6zrOBx9/38wZBKUDrbs6nbVH2McBF72ub8E/suNStnvkFmN5vJt8MEdgA/ONxJOCNgxLnhRnr85EBdJYdt98rGIwIDAQABAoIBAAL/4+F9YTvqEZvHFVlA9JMXIob4mTxoM40O7YQsU1Cf6WerK1G451KYeFrTgYGmAP8ZqXuoKlPpwK0n3EQ3KPgVNTWV43U0m+nF9R7I4RAtJJa0R/nDJW2Mrewyj73vgTGGpGemlTBwjHLKDaN0y4o2r3SucIWWhTeO4meP60CmDHKdNNnDjUjGYHRdWoujc9KodM/ySrJ2/x7P1B+8Rc6Hm1iBV/ivhWfqcMGVPMwdN3u7gSA9GakNggRbYx00LWGQbt/WBYxl/90RwIHGxWLIKfs+0bqTHRrJvYGFhyqOJbaXQJ3MGD1W985of6gBAw57wIGtbv4fpBT3hKcy8AkCgYEA7jQozFSgVVBBgHd2xCENLRZ/k8BhgxA9kA1o6xzHnrDkfQtHEMBUAncE2p2amkXdTd6yASyFOJhvAmSFbgtphkXzB92IKmCvkXaTgH+GEh/x8s5ZAhekenR+BQyJePuNcVrXU7+4NLtki3GZPgAn12RLFMHY9gKUQx5EceiRH+0CgYEAyiwBO+6VDmdz/OUlwhRQd0n8UipdImk1tTbp4+dKc4bSpVHJDklFkEeFq3mxIIFbVxOWQbIpWJuDPNGBAwNudzQElGsVNQA5GCEJNJzr5EMb8vKcjf/OthJrsglEbaNE6qM02qRUyQIXImpXXG13+DrA3kvQKcbQjYIXt4kUHE8CgYEAnqyQIMyeoTX87B6oNq3toQNDqKCWVEAVQkgsg7GAJ+2dxs4kYLw9OgcebKJfwjSX3q9h0/ZcJ/8is21SlKN1f8RIyAyCD/in+zYJu6c3IAu3mA5srEAjiI7hA2v+h2DKL6Bbn6YuhrHtoBXkBrED2S8t9H3MkNPW/37CCT7qrWECgYEAh4IdRaVxpRj0ZmLU/xQMpxghFpWsnT71r8Ph208Q2QRaNlLuNzQne0BlOP01Gvt5VJEdWmVnTyvVODOYjIOFTELCexSXP1Ip8qFaATjY58OPfTTeeFMoo2MPamLJyc7wh3DjIVWaOqy7AudSLPJ2whvnYFhia04GRYMYEH3BzAMCgYBC5worloe1H/V364a6lMWUldO+mxk26EXtKIIMnnpG/BJxoVofbgZMMX1chnRhPsLUZnFhVNo6vrk2it/bE0PGVxtIyO5qYrn/Be+Bn+OUEO7jIL3MQ6iOoHf/favJxsR9mI65xC9ZlQxq8truhX69Z6U+zN6nnQ7R8BHbNW5Wtw==";
    private $charset = "UTF-8";
    private $signType = "RSA2";
    private $returnUrl = "http://115.159.192.243/qy/Trade/Payment/alipayReturn";
    private $notifyUrl = "http://115.159.192.243/qy/Trade/Notify/alipay";
        
    //沙箱的参数
    private $appid="2016080800193517";
    private $gatewayUrl="https://openapi.alipaydev.com/gateway.do";
    private $alipayPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAw7D+CxtfL2a7Vj5r+Q9hyxt3sczZGPo5vt78TZKu5gde5wMiX+v/cxDlR7OYpOtKzMS6tqVIg/pJ4vYfX7eXwRhhiatH0e8UbHSPWMWV7zam8YHtSTwLkI+/gjPbvGxuk3dNGWM3AIqzq2wl06mmVz2W37cRhl9oKc9jLzMIesYFXp9hurU+uKPq4ewZlcsUmYTtinq8q0BomCiZKjNt3EZfQ3JT7GfUgCGhCegLAO4IYFBvhxNr94Ex5CrKh0CyqTKWFdSnzobNCBX4ea9ifLn3YT2W1InEzRu+HqSm4Zmzr9N7IsigpMQAIaN6ohTkMWZLtZ9IAdVE30Hoozc70wIDAQAB";
    
    //应用签约2.0的参数
    // private $appid="2017090508564631";
    // private $gatewayUrl="https://openapi.alipay.com/gateway.do";
    // private $alipayPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlTRTJhHr2Ghn7UxkZpCGE94mAMjuOO403bvDdTjS4mQsNYuwIrw8v1fuLGirH/S71Cfq+0p6VYf1CLwOYuOu3O12sg/t869lbTq3UvyrMXy6RCo5npQCZq2qZ3b3NE+DuL4PQVzfQF8Gr2uzmrsVOQfcCpb9OYHoTjXcuu+O0vP270JPt+8pap65E28x3NyqaqU0gVu6RVkLT7tamdz5nR/jQa9Tit0Wzo0h+bqK6a1mD5Tu0UC45TTPXxSWQzBmOKWzHfCwcsrAcCy5RQDjMfeuU24zRhC3aG9DgURoAwmN0JkDaRs/AC6W3vFRXtdDMX7KfU/wM6CbpRa+HQnRgwIDAQAB";
    
    // //QY直播的参数
    // private $appid="2018010201512085";
    // private $gatewayUrl="https://openapi.alipay.com/gateway.do";

    public $tradeModel;
    private $sdkPath="plugins/pay/vendor/Alipay";
    //AopClient对象
    public function getAopClient(){
        Loader::import("AopClient", $this->sdkPath);
        $aop = new \AopClient();
        $aop->appId = $this->appid;
        $aop->gatewayUrl = $this->gatewayUrl;
        $aop->rsaPrivateKey = $this->privateKey;
        $aop->alipayrsaPublicKey = $this->alipayPublicKey;
        $aop->apiVersion = '1.0';
        $aop->signType = $this->signType;
        $aop->postCharset= $this->charset;
        $aop->format = 'json';
        return $aop;
    }
    //将PaymentModel->createAsyncTrade返回的字段名替换成Alipay请求中的字段名
    public function getContent(){
        $data=$this->tradeModel->record;
        $content=[
            "out_trade_no"=>$data["trade_no"],
            "subject"=>$data["title"],
            "total_amount"=>$data["money"],
            "body"=>$data["description"]
        ];
        return $content;
    }
    //接受电脑网页的交易请求并跳转到支付页面
    public function pagepay(){
        $content=$this->getContent();
        $content["product_code"]="FAST_INSTANT_TRADE_PAY";
        Loader::import("AlipayTradePagePayRequest", $this->sdkPath."/request");
        $request = new \AlipayTradePagePayRequest();
        $request->setReturnUrl($this->returnUrl);
        $request->setNotifyUrl($this->notifyUrl);
        $request->setBizContent(json_encode($content));
        $result = $this->getAopClient()->pageExecute($request);
        return $result;
    }
    //接受手机APP的交易请求并且生成签名
    public function signature(){
        $content=$this->getContent();
        $content["product_code"]="QUICK_MSECURITY_PAY";
        $content["timeout_express"]="30m";
        Loader::import("AlipayTradeAppPayRequest", $this->sdkPath."/request");
        $request = new \AlipayTradeAppPayRequest();
        $request->setNotifyUrl($this->notifyUrl);
        $request->setBizContent(json_encode($content));
        $result = $this->getAopClient()->sdkExecute($request);
        return $result;
        //REMBER:实际输出时不要加下面这句
        //return htmlspecialchars($result);
    }
    //查询接口
    public function query($trade_no=null,$alipay_trade_no=null){
        $content=[
            "out_trade_no"=>$trade_no
        ];
        Vendor('Alipay.request.AlipayTradeQueryRequest');
        Vendor('Alipay.SignData');
        $request = new \AlipayTradeQueryRequest();
        $request->setBizContent(json_encode($content));
        $result = $this->getAopClient()->execute($request);
        return $result->alipay_trade_query_response;
    }
    public function check($data,$type="notify"){
        $data["sign"]=str_replace(" ","+",$data["sign"]);
        $result = $this->getAopClient()->rsaCheckV1($data, $this->alipayPublicKey, $this->signType);
        Log::record("[Alipay] result:".($result?"success":"fail")." type:$type appid:".$this->appid,Log::DEBUG);
        if(!$result){
            $this->error="验签失败";
            Log::record("[Alipay] error:".$this->error." appid2:".$data["app_id"],Log::DEBUG);

            $params=$this->query($data["out_trade_no"]);
            Log::record("[WeChat] 主动查询结果:".$params->code."  失败原因:".$params->sub_msg,Log::DEBUG);
            if($params->code!=="10000"){
                return false;
            }
        }
        return $data;
    }
    public function verifyReturn($data){
        $result=$this->check($data,"return");
        if(!$result){
            return false;
        }
        return BaseModel::updateAsyncTrade($content["out_trade_no"],$content["transaction_id"]);
    }
    public function verifyNotify(){
        return $this->check($_POST);
    }
    public function replyNotify($result,$message=""){
        echo $result ? "success" : "fail";
        exit;
    }
    
    // //http://127.0.0.1/huli/index.php?g=Trade&m=Index&a=doTransfer&sandbox=1
    // public function transfer($biz_no,$account,$realname,$amount,$title,$remark){
    //     Vendor('Alipay.request.AlipayFundTransToaccountTransferRequest');
    //     Vendor('Alipay.SignData');
    //     $request = new \AlipayFundTransToaccountTransferRequest();
    //     $content=[
    //         "out_biz_no"=>$biz_no,//"3142321423432",
    //         "payee_type"=>"ALIPAY_LOGONID",
    //         "payee_account"=>$account,//"bqeuqn3143@sandbox.com",
    //         "amount"=>$amount,//"12.23",
    //         "payer_show_name"=>$title,//"上海交通卡退款",
    //         "payee_real_name"=>$realname,//"沙箱环境",
    //         "remark"=>$remark//"转账备注"
    //     ];
    //     $request->setBizContent(json_encode($content));
    //     return $this->getAopClient()->execute($request);
    // }
}