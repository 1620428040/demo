<?php
namespace plugins\pay\controller;
use cmf\controller\PluginBaseController;
use plugins\pay\model\BaseModel;
use plugins\pay\logic\PayLogic;
use plugins\pay\model\AlipayModel;

//测试支付宝页面支付的页面，并不能实际使用
class IndexController extends PluginBaseController{
    public function index(){
        echo "ok";
    }
    //显示电脑网页支付界面
    public function pagepay(){
        return $this->fetch();
    }
    //接受电脑网页的交易请求并跳转到支付页面
    public function doPagepay(){
        return BaseModel::getModel("")->createAsyncTrade("0",$_POST)->alipay()->pagepay();
    }
    //同步回调，验证交易信息，并显示交易成功页面
    public function returnPage(){
        $pay=new AlipayModel();
        $result=$pay->verifyReturn($_GET);
        if(!$result){
            return "验证失败";
        }
        return "交易成功";
    }

    //充值界面
    public function topup(){
        $this->display();
    }
    public function doTopup(){
        $uid=I("uid","");
        $money=I("money");
        $data=M("Users")->where(["id"=>$uid])->find();
        if(!$data){
            $this->error("找不到用户");
        }
        if(!$money){
            $this->error("充值金额不能为空");
        }
        $model=D("Topup");
        $data=$model->createAsyncTrade($uid,"Alipay",["money"=>$money]);
        if(!$data){
            $this->error($model->getError());
            return false;
        }

        $alipay= D("Alipay");
        echo $alipay->pagepay($data);
    }

    

    //电脑网页交易查询页面
    public function query(){
        $this->display();
    }
    //查询接口
    public function doQuery(){
        $out_trade_no=I("out_trade_no","");
        $trade_no=I("trade_no","");

        $record=D("Alipay")->query($out_trade_no,$trade_no);
        echo json_encode($record,JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT);
    }

    public function withdraw(){
        $this->display();
    }
    public function doWithdraw(){
        $uid=I("uid","");
        $account=I("account","");
        $realname=I("realname","");
        $amount=intval(I("amount","0"));

        $result=D("Pay")->withdrawToAlipay($uid,$account,$realname,$amount);
        echo "<pre>".json_encode($result,JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT)."</pre>";
    }
}
