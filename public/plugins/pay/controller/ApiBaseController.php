<?php
namespace Trade\Controller;
use Common\Controller\ApiController;

class PaymentController extends ApiController{
    protected $tradeModel="";
    public function getOrderParamsInput(){
        return [];
    }
    
    public function getAlipaySignature(){
        $user=$this->checkUser();
        $model=BaseModel::getModel("")->createAsyncTrade($user["id"],$_POST);
        $data=$model->alipay()->signature();
        $this->showData($data);
    }
    //页面支付用的版本，暂时隐藏
    // public function alipayReturn(){
    //     $pay=D("Alipay");
    //     $content = $pay->verifyReturn($_GET);
    //     if(!$content){
    //         $this->error($pay->getError());
    //     }

    //     $model=D($this->tradeModel);
    //     $result=$model->updateAsyncTrade($content["out_trade_no"],$content["transaction_id"],"return");
    //     if($result===0||$result===1){
    //         $this->success($model->info);
    //     }
    //     else{
    //         $this->error($model->info);
    //     }
    // }
    public function alipayReturn(){
        // $user=$this->checkUser();
        $pay=D("Alipay");
        $content = $pay->verifyReturn($_POST);
        if(!$content){
            $this->error($pay->getError());
        }

        $model=D($this->tradeModel);
        $result=$model->updateAsyncTrade($content["out_trade_no"],$content["transaction_id"],"return");
        if($result===0||$result===1){
            $this->success($model->info);
        }
        else{
            $this->error($model->info);
        }
    }
    public function getWeChatOrder(){
        $user=$this->checkUser();
        $params=$this->getOrderParamsInput();
        $model=D($this->tradeModel);
        $data=$model->createAsyncTrade($user["id"],"WeChat",$params);
        if(!$data){
            $this->error($model->getError());
        }

        $wx= D("WeChatPay");
        $order=$wx->createOrder($data);
        if(!$order){
            $this->error($wx->getError());
        }
        $this->showData($order);
    }
    public function weChatReturn(){
        $tradeNo=I("trade_no");
        $outTradeNo=I("out_trade_no","");
        $user=$this->checkUser();

        $wx= D("WeChatPay");
        $result=$wx->verifyReturn(["trade_no"=>$tradeNo,"out_trade_no"=>$outTradeNo]);
        if(!$result){
            $this->error($wx->getError());
        }

        $model=D($this->tradeModel);
        $result=$model->updateAsyncTrade($tradeNo,$outTradeNo,"return");
        if($result===0||$result===1){
            $this->success($model->info);
        }
        else{
            $this->error($model->info);
        }
    }
    public function verifyAppleTrade(){
        $user=$this->checkUser();
        $receipt=I("receipt-data");
        if(!$receipt){
            $this->error("验证参数不能为空");
        }
        
        //TODO:如果必要的话，可以用单独的ApplePay Model封装验证的方法
        // $apple=D("ApplePay");
        // $sandbox = I("sandbox") ? true : false ;
        // $response=$apple->verify($receipt,$sandbox);
        $url = I("sandbox") ? "https://sandbox.itunes.apple.com/verifyReceipt" : "https://buy.itunes.apple.com/verifyReceipt" ;
        $curl = new \Curl();
        $result = $curl->post($url, json_encode(["receipt-data"=>$receipt]));
        $response = json_decode($result,true);
        if(!$response){
            $this->error("解析服务器响应失败");
        }
        if($response["status"]!==0){
            $this->error("验单失败");
        }
        $params=$response["receipt"]["in_app"][0];
        $outTradeNo=$params["transaction_id"];
        
        $model=D($this->tradeModel);
        $result=$model->createSyncTrade($user["id"],"apple",$outTradeNo,$params);
        if(!$result){
            $this->error("保存订单失败");
        }
        $this->showData($result);
    }
}