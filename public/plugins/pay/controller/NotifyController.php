<?php
namespace Trade\Controller;
use Think\Controller;
use Think\Log;

//用来接收回调消息
class NotifyController extends Controller{
    //支付宝回调，验证交易信息，并将结果返回给支付宝服务器
    public function alipay(){
        $pay=D("Alipay");
        $content = $pay->verifyNotify();
        if(!$content){
            $pay->replyNotify(false,$pay->getError());
        }

        $model=D("Payment");
        $result=$model->updateAsyncTrade($content["out_trade_no"],$content["transaction_id"],"return");
        if($result!==4){
            //虽然交易可能没成功，但是已经有记录了，不需要再接收相同的信息
            $pay->replyNotify(true,$model->info);
        }
        else{
            $pay->replyNotify(false,$model->info);
        }
    }
    public function weChat(){
        $pay=D("WeChatPay");
        $content = $pay->verifyNotify();
        if(!$content){
            $pay->replyNotify(false,$pay->getError());
        }

        $model=D("Payment");
        $result=$model->updateAsyncTrade($content["out_trade_no"],$content["transaction_id"],"return");
        if($result!==4){
            //虽然交易可能没成功，但是已经有记录了，不需要再接收相同的信息
            $pay->replyNotify(true,$model->info);
        }
        else{
            $pay->replyNotify(false,$model->info);
        }
    }
    //获取
    public function getPaymentModel($tradeNo){
        if(strpos($tradeNo,"topup")){
            return D("TopupModel");
        }
        elseif(strpos($tradeNo,"noble")){
            return D("NobleModel");
        }
        elseif(strpos($tradeNo,"member")){
            return D("MemberModel");
        }
    }
}
