<?php
namespace Trade\Controller;

class TopupController extends PaymentController{
    protected $tradeModel="Topup";
    public function getOrderParamsInput(){
        $money = I('money');
        if(!$money){
            $diamond=I("diamond");
            if(!$diamond){
                $this->error("充值金额不能为空");
            }
            $money=$diamond/10;
        }
        return ["money"=>$money];
    }
}