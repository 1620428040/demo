<?php
namespace plugins\pay;
use cmf\lib\Plugin;
use think\Db;

class PayPlugin extends Plugin{
    public $info = [
        'name'=>'Pay',
        'title'=>'支付',
        'description'=>'支持支付宝、微信支付、iOS内购等',
        'status'=>1,
        'author'=>'guodong',
        'version'=>'0.1',
        'demo_url'=>''
    ];
    public $hasAdmin = 1;//插件是否有后台管理界面

    public function install(){
        $prefix=config("database.prefix");
        //支付记录表
        $trade_table_name=$prefix."pay_trade";
        $sql="CREATE TABLE IF NOT EXISTS `$trade_table_name` ( 
            `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT , 
            `trade_no` VARCHAR(30) NOT NULL , 
            `out_type` VARCHAR(20) NOT NULL , 
            `out_trade_no` VARCHAR(30) NULL , 
            `trade_type` VARCHAR(20) NOT NULL , 
            `title` VARCHAR(20) NOT NULL , 
            `money` FLOAT(11,2) NOT NULL , 
            `description` VARCHAR(100) NULL , 
            `trade_data` TEXT NULL , 
            `time` DATETIME NOT NULL , 
            `return_status` INT NOT NULL DEFAULT '0' , 
            `notify_status` INT NOT NULL DEFAULT '0' , 
            `status` INT NOT NULL DEFAULT '0' , 
            `message` VARCHAR(20) NULL
        ) ENGINE = MyISAM;";
        Db::execute($sql);
        return true;
    }

    public function uninstall(){
        $prefix=config("database.prefix");
        $trade_table_name=$prefix."pay_trade";
        $sql="DROP TABLE $trade_table_name";
        Db::execute($sql);
        return true;
    }
}