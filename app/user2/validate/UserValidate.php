<?php
namespace app\user2\validate;

use think\Validate;

class UserValidate extends Validate
{
    protected $field = [
        'user_id'=>'用户ID',
        'nickname'=>'昵称',
        'password'=>'密码'
    ];
    protected $rule = [
        'nickname'=>'require',
        'password' =>'require',
    ];
    protected $scene = [
        "register"=>['nickname','password'],
        "login"=>['user_id','nickname','password'],
    ];
    public function check($data, $rules = [], $scene = '')
    {
        
    }
}