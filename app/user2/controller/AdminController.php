<?php
namespace app\user2\controller;

use cmf\controller\AdminBaseController;

/**
 * 后台管理
 * Class AdminController
 * @package app\user2\controller
 * @adminMenuRoot(
 *     'name'   =>'扩展用户管理',
 *     'action' =>'default',
 *     'parent' =>'',
 *     'display'=> true,
 *     'order'  => 30,
 *     'icon'   =>'th',
 *     'remark' =>''
 * )
 */
class AdminController extends AdminBaseController
{
    /**
     * 主页面
     * @adminMenu(
     *     'name'   => '用户管理',
     *     'parent' => 'user2/Admin/default',
     *     'display'=> true,
     *     'hasView'=> true,
     *     'order'  => 1,
     *     'icon'   => '',
     *     'remark' => '主页面',
     *     'param'  => ''
     * )
     */
    public function index()
    {
        return "index";
    }
}