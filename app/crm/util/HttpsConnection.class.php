<?php
namespace Common\Util;
use Think\Log;

/**
 * 通过HTTPS协议进行服务器间通讯
 * 默认通过JSON格式进行交互
 * 
 * 如果要用query string格式，要用到的函数
 * $content=http_build_query($data);
 * parse_str($content,$data);
 */
class HttpsConnection{
    protected $error="";
    public function getError(){
        return $this->error;
    }
    //将要发送的数据转换成对方服务器指定格式的内容(包括数据加密)
    protected function stringifyData($data){
        $content=json_encode($data);
        return $content;
    }
    //将对方服务器发送的内容解析成数据(包括数据解密)
    protected function parseData($content){
        $data=json_decode($content,true);
        return $data;
    }
    //处理主动发起请求时，对方服务器的响应。如果对方返回错误信息则用统一的接口
    protected function disposeResponse($content){
        if(!$content){
            Log::record("disposeResponse error : 返回的内容为空",Log::ERR);
            return false;
        }
        $data=$this->parseData($content);
        return $data;
    }
    //添加签名
    protected function getSignature(){
        return "";
    }
    //验证签名
    protected function verifySignature($data){
        return $data;
    }
    //GET方式发送请求
    protected function sendGetRequest($url){
        Log::record("sendGetRequest:".$url,Log::DEBUG);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30); // 设置超时限制防止死循环
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $output = curl_exec($ch);
        if(!$output){
            Log::record("curl get error:".curl_errno($ch)." message:".curl_error($ch),Log::ERR);
            $this->error="curl get error";
            return false;
        }
        curl_close($ch);
        return $output;
    }
    //POST方式发送请求
    protected function sendPostRequest($url,$data){
        Log::record("sendPostRequest:".$url,Log::DEBUG);
        Log::record("post data:".$data,Log::DEBUG);
        $ch = curl_init(); // 启动一个CURL会话
        curl_setopt($ch, CURLOPT_URL, $url); // 要访问的地址
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // 获取的信息以文件流的形式返回
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); // 对认证证书来源的检查
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0); // 从证书中检查SSL加密算法是否存在
        curl_setopt($ch, CURLOPT_POST, 1); // 发送一个常规的Post请求
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data); // Post提交的数据包
        curl_setopt($ch, CURLOPT_TIMEOUT, 30); // 设置超时限制防止死循环
        curl_setopt($ch, CURLOPT_HEADER, 0); // 显示返回的Header区域内容
        $output = curl_exec($ch); // 执行操作
        if(!$output){
            Log::record("curl post error:".curl_errno($ch)." message:".curl_error($ch),Log::ERR);
            $this->error="curl post error";
            return false;
        }
        curl_close($ch); // 关闭CURL会话
        return $output;
    }
    //主动发起请求
    public function get($url){
        $output=$this->sendGetRequest($url);
        $data=$this->disposeResponse($output);
        return $data;
    }
    public function post($url,$data){
        $data=$this->stringifyData($data);
        $output=$this->sendPostRequest($url,$data);
        $data=$this->disposeResponse($output);
        return $data;
    }
    //接收对方服务器的请求(回调请求)，并回复
    public function receive(){
        $params=$_GET;
        Log::record("receive get=".json_encode($params,JSON_UNESCAPED_UNICODE),Log::DEBUG);
        $content=file_get_contents("php://input");
        Log::record("receive post:".$content,Log::DEBUG);
        $data=$this->parseData($content);
        $data=$this->verifySignature($data);
        $params["data"]=$data;
        Log::record("receive data=".json_encode($params,JSON_UNESCAPED_UNICODE),Log::DEBUG);
        return $params;
    }
    public function reply($data){
        Log::record("reply data=".json_encode($data,JSON_UNESCAPED_UNICODE),Log::DEBUG);
        $result=$this->stringifyData($data);
        Log::record("reply stringify:".$result,Log::DEBUG);
        echo $result;
        exit;
    }
}