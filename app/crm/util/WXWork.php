<?php
namespace app\crm\util;
use think\Model;
use think\Log;
use think\Db;

//向企业微信API发送请求，处理企业微信的回调的模型
class WXWork extends HttpsConnection{
    public $corpID;
    public $agentID;
    public $secret;
    public $domain;
    public $token;
    public $ticket;
    public $wxqy="https://qyapi.weixin.qq.com/cgi-bin/";
    function __construct()
    {
        $this->domain=config("Domain");
        $this->corpID=config("WXWork_CorpID");
        $this->agentID=config("WXWork_AgentId");
        $this->secret=config("WXWork_Secret");
    }
    public function sendGetRequest($url)
    {
        $output=parent::sendGetRequest($url);
        $data=json_decode($output,true);
        return $data;
    }
    public function getToken()
    {
        if($this->token){
            return $this->token;
        }
        $token=cache("WXWorkToken");
        if(!$token){
            $url=$this->wxqy."gettoken?corpid=".$this->corpID."&corpsecret=".$this->secret;
            $response=$this->sendGetRequest($url);
            $token=$response["access_token"];
            cache("WXWorkToken",$token,["expire"=>$response["expires_in"]]);
        }
        $this->token=$token;
        return $token;
    }
    public function getJSApiTicket()
    {
        if($this->ticket){
            return $this->ticket;
        }
        $ticket=S("WXWorkTicket");
        if(!$ticket){
            $url=$this->wxqy."get_jsapi_ticket?access_token=".$this->getToken();
            $response=$this->get($url);
            $ticket=$response["ticket"];
            S("WXWorkTicket",$ticket,["expire"=>$response["expires_in"]]);
        }
        $this->ticket=$ticket;
        return $ticket;
    }
    public function generateSignature(&$nonce,&$timestamp,&$url)
    {
        $ticket=$this->getJSApiTicket();
        $nonce=rand(100000,999999);
        $timestamp=time();
        $url=$this->domain.$_SERVER['REQUEST_URI'];
        $string="jsapi_ticket=$ticket&noncestr=$nonce&timestamp=$timestamp&url=$url";
        $signature=sha1($string);
        // var_dump($ticket,$nonce,$timestamp,$url,$string,$signature);
        return $signature;
    }
    /**
     * 获取企业信息的接口
     */
    public function getDepartmentList($id="")
    {
        $url=$this->wxqy."department/list?access_token=".$this->getToken();
        if($id){
            $url.="&id=".$id;
        }
        $response=$this->get($url);
        if(!$response){
            return false;
        }
        return $response["department"];
    }
    public function getSimpleUserList($departmentID,$fetchChild=0)
    {
        $url=$this->wxqy."user/simplelist?access_token=".$this->getToken()."&department_id=$departmentID&fetch_child=$fetchChild";
        $response=$this->get($url);
        return $response["userlist"];
    }
    public function getUserList($departmentID,$fetchChild=0)
    {
        $url=$this->wxqy."user/list?access_token=".$this->getToken()."&department_id=$departmentID&fetch_child=$fetchChild";
        $response=$this->get($url);
        return $response["userlist"];
    }
    public function getUser($userID)
    {
        $token=$this->getToken();
        $url="https://qyapi.weixin.qq.com/cgi-bin/user/get?access_token=$token&userid=$userID";
        $response=$this->get($url);
        return $response;
    }
    /**
     * 其他功能
     */
    public function sync()
    {
        $token=$this->getToken();
        // $db=Db::getInstance();

        $url=$this->wxqy."department/list?access_token=$token";
        $response=$this->sendGetRequest($url);
        if(!$response){
            return false;
        }
        $departmentList=$response["department"];
        $values=[];
        foreach($departmentList as $department){
            $values[]="(".$department["id"].",'".$department["name"]."',".$department["parentid"].",".$department["order"].")";
        }
        $sql="TRUNCATE `ww_department`;\n";
        $sql.="INSERT INTO `ww_department` (`id`, `name`, `parentid`, `order`) VALUES ".join(",",$values).";\n";
        
        $url=$this->wxqy."user/list?access_token=$token&department_id=1&fetch_child=1";
        $response=$this->sendGetRequest($url);
        if(!$response){
            return false;
        }
        $userList=$response["userlist"];
        $values=[];
        $user_department=[];
        foreach($userList as $user){
            $userID=$user["userid"];
            $values[]="('$userID', '".$user["name"]."', '".$user["mobile"]."', '".$user["position"]."', '".$user["gender"]."', '".$user["email"]."', '".$user["isleader"]."', '".$user["avatar"]."', '".$user["telephone"]."', '".$user["english_name"]."', '".$user["status"]."', '".$user["qr_code"]."')";
            foreach($user["department"] as $index=>$departmentID){
                $order=$user["order"][$index];
                $user_department[]="('$userID', '$departmentID', '$order')";
            }
        }
        $sql.="TRUNCATE `ww_user`;\n";
        $sql.="INSERT INTO `ww_user` (`userid`, `name`, `mobile`, `position`, `gender`, `email`, `isleader`, `avatar`, `telephone`, `english_name`, `status`, `qr_code`) VALUES ".join(",",$values).";\n";
        $sql.="TRUNCATE `ww_user_department`;\n";
        $sql.="INSERT INTO `ww_user_department` (`userid`, `department`, `order`) VALUES ".join(",",$user_department).";\n";
        
        Db::getConnection()->connect()->exec($sql);
        return true;
    }
    public function getUserDepartmentTree($id="1")
    {
        $department=D("Department");
        $view=D("UserDepartmentView");
        $root=$department->where(["id"=>$id])->find();
        $tree=$this->recursionUserDepartmentNode($department,$view,$root);
        return $tree;
    }
    private function recursionUserDepartmentNode($department,$view,$node)
    {
        $id=$node["id"];
        $node["userList"]=$view->where(["department"=>$id])->select();
        $node["subList"]=[];
        $subList=$department->where(["parentid"=>$id])->select();
        foreach($subList as $sub){
            $node["subList"][]=$this->recursionUserDepartmentNode($department,$view,$sub);
        }
        return $node;
    }
}