<?php
namespace app\crm\controller;
use think\Controller;
use app\crm\util\WXWork;

class IndexController extends Controller
{
    public function index()
    {
        return "hello world !";
    }
    public function getToken()
    {
        $wx=new WXWork;
        var_dump($wx->getToken());
    }
    public function sync()
    {
        $wx=new WXWork;
        var_dump($wx->sync());
    }
}
// namespace CRM\Controller;
// use Think\Controller;
// use CRM\Util\WXWork;
// use Think\Log;

// class IndexController extends Controller{
//     public function _initialize(){
//         //禁止浏览器缓存
//         header('Cache-Control:no-cache,must-revalidate');
//         header('Pragma:no-cache');
//         header("Expires:0");
//     }
//     //正常进入的主页
//     //http://127.0.0.1/wxwork/index.php/CRM/Index/index/userID/GuoDong
//     public function index(){
//         $wx=new WXWork;
//         $this->assign("token",$wx->getToken());
//         if(isset($_GET["userID"])){
//             $userID=$_GET["userID"];
//         }
//         else{
//             $user=session("user");
//             if(!$user){
//                 $this->show('<script>location.href="'.U("portal").'"</script>');
//                 exit;
//             }
//             $userID=$user["userid"];
//         }
//         $userModel=D("User");
//         $user=$userModel->where(["userid"=>$userID])->find();//$wx->getUser($userID);
//         $this->assign("user",$user);
//         $this->display();
//     }
//     //不是企业用户时显示的页面
//     public function outside(){
//         $this->show("您不是企业内部用户");
//     }
//     //通过企业微信获取当前用户
//     public function portal(){
//         $url="";
//         $wx=new WXWork;
//         $user=session("user");
//         if($user){
//             $url=$wx->getWholeUrl("index");
//         }
//         else{
//             $redirect=$wx->getWholeUrl("route");
//             $url=$wx->getAuthUrl($redirect,"snsapi_privateinfo");
//         }
//         // $this->show($url);
//         $this->show('<script>location.href="'.$url.'"</script>');
//     }
//     //获取到用户信息后进行重定向
//     //$_GET={"code":"Ri1fRNKoxJnqsiectqOvPszDgzg7Sls5PyG1rn84d2A","state":"\"\""}
//     public function route(){
//         $wx=new WXWork;
//         $info=$wx->getUserInfo($_GET["code"]);
//         if(!$info){
//             $this->error($wx->getError());
//         }
//         if(!isset($info["userID"])){
//             $this->show('<script>location.href="'.U("outside").'"</script>');
//             exit;
//         }
//         $userModel=D("User");
//         $user=$userModel->where(["userid"=>$info["userID"]])->find();
//         session("user",$user);
//         $this->show('<script>location.href="'.U("index").'"</script>');
//     }
//     public function getInfo(){
//         $wx=new WXWork;
//         var_dump($wx->getDepartmentList());
//         var_dump($wx->getSimpleUserList(1));
//         var_dump($wx->getUserList(1));
//         var_dump($wx->getToken());
//     }
//     public function sync(){
//         $wx=new WXWork;
//         var_dump($wx->sync());
//     }
//     //人员、部门选择页面
//     public function select(){
//         $id=I("id",1);
//         $wx=new WXWork;
//         $tree=$wx->getUserDepartmentTree($id);
//         $this->assign("tree",$tree);
//         $this->display();
//     }
//     public function sendMessage(){
//         $wx=new WXWork;
//         $wx->sendMessage();
//     }
// }