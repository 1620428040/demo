<?php
namespace app\crm\controller;

use cmf\controller\RestBaseController;


class CustomerController extends RestBaseController{
    public function listAll(){
        $customerModel=D("Customer");
        $customerList=$customerModel->select();
        $this->assign("type","all");
        $this->assign("list",$customerList);
        $this->display("list");
    }
    public function listICreated(){

    }
    public function listIRespon(){

    }
    public function listIRelated(){

    }
    public function create(){
        $this->assign("sign","create");
        $this->assign("customer",[]);
        $this->display("edit");
    }
    public function modify(){
        $id=I("id");
        if(!$id){
            $this->error("客户ID不能为空");
        }
        $customerModel=D("Customer");
        $customer=$customerModel->where(["id"=>$id])->find();
        if(!$customer){
            $this->error("不存在指定的客户");
        }
        $this->assign("sign","modify");
        $this->assign("customer",$customer);
        $this->display("edit");
    }
    public function handover(){
        $this->error("开发中");
    }
    public function doEdit(){
        $customerModel=D("Customer");
        $sign=I("sign","create");
        $data=$_POST;
        if($sign==="create"){
            unset($data["id"]);
            $data["create_time"]=time();
            $result=$customerModel->add($data);
        }
        else{
            $id=$data["id"];
            unset($data["id"]);
            $result=$customerModel->where(["id"=>$id])->save($data);
        }
        if($result===false){
            $this->error($customerModel->getError());
        }
        $this->success("保存成功",U("listAll"));
    }
}