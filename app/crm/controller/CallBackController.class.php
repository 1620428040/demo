<?php
namespace CRM\Controller;
use Think\Controller;
use CRM\Util\WXWork;
use CRM\Util\WXWorkMessage;

//处理企业微信服务器的回调请求
class CallBackController extends Controller{
    public function index(){

    }
    //http://127.0.0.1/wxwork/index.php/CRM/CallBack/receiveMessage
    public function receiveMessage(){
        $wx=new WXWorkMessage;
        if(IS_GET){
            $wx->testifyMessageInterface($_GET);
        }
        else{
            $message=$wx->receiveMessage();
            $wx->replyMessage("这是一条测试信息:自动回复:$message");
        }
    }
}