<?php
namespace CRM\Controller;
use Think\Controller;
use CRM\Util\WXWork;

class WXWorkController extends Controller{
    public $wx;
    public function _initialize(){
        $wx=new WXWork;
        $sign=$wx->generateSignature($nonce,$timestamp,$url);
        $this->assign("nonce",$nonce);
        $this->assign("timestamp",$timestamp);
        $this->assign("url",$url);
        $this->assign("signature",$sign);
        $this->assign("corpID",$wx->corpID);
        $this->wx=$wx;
    }
}