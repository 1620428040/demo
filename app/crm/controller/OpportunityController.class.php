<?php
namespace CRM\Controller;

class OpportunityController extends WXWorkController{
    public function listAll(){
        $model=D("Opportunity");
        $list=$model->select();
        $this->assign("type","all");
        $this->assign("list",$list);
        $this->display("list");
    }
}