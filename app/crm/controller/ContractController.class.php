<?php
namespace CRM\Controller;

class ContractController extends WXWorkController{
    public function listAll(){
        $model=D("Contract");
        $list=$model->select();
        $this->assign("type","all");
        $this->assign("list",$list);
        $this->display("list");
    }
}