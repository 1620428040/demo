<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2018 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 小夏 < 449134904@qq.com>
// +----------------------------------------------------------------------
namespace app\crm\controller;

use cmf\controller\AdminBaseController;
use app\crm\util\WXWork;

/**
 * Class AdminCrmController
 * @package app\crm\controller
 * @adminMenuRoot(
 *     'name'   =>'CRM应用',
 *     'action' =>'default',
 *     'parent' =>'',
 *     'display'=> true,
 *     'order'  => 30,
 *     'icon'   =>'th',
 *     'remark' =>'企业微信CRM应用'
 * )
 */
class AdminCrmController extends AdminBaseController
{
    /**
     * 从企业微信同步数据
     * @adminMenu(
     *     'name'   => '同步数据',
     *     'parent' => 'crm/AdminCrm/default',
     *     'display'=> true,
     *     'hasView'=> true,
     *     'order'  => 1,
     *     'icon'   => '',
     *     'remark' => '从企业微信同步数据',
     *     'param'  => ''
     * )
     */
    public function sync()
    {
        $wx=new WXWork;
        $result=$wx->sync();
        if(!$result){
            return "操作失败";
        }
        return "操作成功";
    }
}