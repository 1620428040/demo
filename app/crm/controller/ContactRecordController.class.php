<?php
namespace CRM\Controller;

class ContactRecordController extends WXWorkController{
    public function listAll(){
        $model=D("ContactRecord");
        $list=$model->select();
        $this->assign("type","all");
        $this->assign("list",$list);
        $this->display("list");
    }
    public function listICreated(){

    }
    public function listIRespon(){

    }
    public function listIRelated(){

    }
    public function create(){
        $this->assign("sign","create");
        $this->assign("record",[]);
        $this->display("edit");
    }
    public function modify(){
        $id=I("id");
        if(!$id){
            $this->error("客户ID不能为空");
        }
        $model=D("ContactRecord");
        $record=$model->where(["id"=>$id])->find();
        if(!$record){
            $this->error("不存在指定的客户");
        }
        $this->assign("sign","modify");
        $this->assign("record",$record);
        $this->display("edit");
    }
    public function handover(){
        $this->error("开发中");
    }
    public function doEdit(){
        $model=D("ContactRecord");
        $sign=I("sign","create");
        $data=$_POST;
        if($sign==="create"){
            unset($data["id"]);
            $data["create_time"]=time();
            $result=$model->add($data);
        }
        else{
            $id=$data["id"];
            unset($data["id"]);
            $result=$model->where(["id"=>$id])->save($data);
        }
        if($result===false){
            $this->error($model->getError());
        }
        $this->success("保存成功",U("listAll"));
    }
}