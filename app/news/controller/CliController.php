<?php
namespace app\news\controller;
use app\common\util\DomUtil;
use app\portal\model\PortalPostModel;

/**
 * 命令行操作
 * CHCP 65001
 * d:
 * cd D:\wamp64\www\thinkcmf\public\
 * php -f index.php news/cli/
 */
class CliController{
    public $bbc_url="http://feeds.bbci.co.uk/news/rss.xml";
    public function __construct(){
        config('app_trace',false);
    }
    public function index(){
        echo "ok";
    }
    public function get(){
        echo "start get news.\n";
        $curl=curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->bbc_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        $output = curl_exec($curl);
        curl_close($curl);

        $dom = new DomUtil('1.0', 'UTF-8');
        $dom->loadXML($output);
        $channel=$dom->getElementsByTagName("channel")->item(0);
        $response=$dom->parseXMLNode($channel);

        $portalPostModel = new PortalPostModel();
        $count=0;

        $user_id="0";
        $categories="1";
        foreach($response["itemList"] as $item){
            $old=$portalPostModel->where(["post_title"=>$item["title"]])->find();
            if(!$old){
                $article=[
                    "user_id"=>$user_id,
                    "post_title"=>$item["title"],
                    "post_source"=>"BBC News",
                    "post_excerpt"=>$item["description"],
                    "post_content"=>$item["description"],
                    "more"=>[
                        "thumbnail"=>$item["thumbnail"]["url"],
                        "guid"=>$item["guid"]["text"]
                    ],
                    "post_keywords"=>"",
                    "published_time"=>$item["pubDate"]
                ];

                $data=$article;
                if (!empty($data['more']['thumbnail'])) {
                    $data['more']['thumbnail'] = cmf_asset_relative_url($data['more']['thumbnail']);
                }
                $portalPostModel->allowField(true)->data($data, true)->isUpdate(false)->save();
                if (is_string($categories)) {
                    $categories = explode(',', $categories);
                }
                $portalPostModel->categories()->save($categories);
                $data['post_keywords'] = str_replace('，', ',', $data['post_keywords']);
                $keywords = explode(',', $data['post_keywords']);
                $portalPostModel->addTags($keywords, $portalPostModel->id);
                $count++;
            }
        }
        echo "$count news stories were added.\n";
    }
    /**
     * 循环执行抓取新闻的操作
     * 这样写容易把进程卡死，还是写定时执行或者node
     * CHCP 65001
     * d:
     * cd D:\wamp64\www\thinkcmf\public\
     * php -f index.php news/cli/sustain
     */
    public function sustain(){
        if(IS_CLI){
            $this->get();
            sleep(30*60);
            $this->sustain();
        }
        else{
            echo "need to execute on the command line.\n";
        }
    }
}